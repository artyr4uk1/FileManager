Properties:
Looking visually like Total Commander (have two windows).
Showing a catalogue inside every window.
Showing an appropriate icon for different file types
Showing files attributes (file name, extension, file size,  date created etc).
Ability to show/hide some file attributes.
Ability to sort files by name, size etc.
Support file/folders operations: copy, move, rename, delete.
Copying should be handled like in Total Commander from one window to another
Showing progress bar during the operation
When file/folder is copying, user still can browse the app.
Non-blocking UI – not freeze while using the app.
Showing dialog windows (like confirm deleting etc.)

Compile for Linux - Makefile
