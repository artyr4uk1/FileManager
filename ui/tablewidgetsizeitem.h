#ifndef TABLEWIDGETSIZEITEM_H
#define TABLEWIDGETSIZEITEM_H

#include <QTableWidgetItem>
#include <vector>
#include <ctime>
#include <string>

// Extends sorting for size column
class TableWidgetSizeItem: public QTableWidgetItem
{
private:
    long long size;
    virtual bool operator<(QTableWidgetItem const &other) const;
public:
    long long getSize() const;
    void setSize(long long value);
};

#endif // TABLEWIDGETSIZEITEM_H
