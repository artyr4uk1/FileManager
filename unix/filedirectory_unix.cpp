#include "filedirectory.h"
#include <string>

namespace filedirectory {

//  Definitions of behaviour functions
using std::string;

string Entity::GetName(){
    return name;
}

time_t Entity::GetDate(){
    return date;
}

string Entity::GetPath(){
    return path;
}

std::string File::GetExtension(){
    return extension;
}

int File::GetSize(){
    return size;
}

string Directory::Open(const std::string &path, const std::string &name){
    string next_path = path + "/" + name;
    return next_path.c_str();
}

string DotDot::Open(const string &path){
    string ret_path = path;
    int pos = ret_path.find_last_of("/");
    ret_path = ret_path.substr(0, pos);
    return ret_path.c_str();
}

}
