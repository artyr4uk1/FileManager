#include "catalogmodel.h"
#include "utils/pulldata.h"
#include "utils/convert.h"

#include <QDebug>
#include <sys/stat.h>
#include <vector>
#include <string>
#include <cassert>
#include <sstream>
#include <iomanip>

namespace catalogmodel {

// Definition of model functions
using std::string;

// Additional functions of pulling data
using pulldata::GetCapacity;
using pulldata::AfterLastSymbol;
using pulldata::IsFile;
using pulldata::IsDir;
using pulldata::ConcatPath;
using pulldata::BeforeLastSymbol;

CatalogModel::CatalogModel(const char *dir_path) : path(dir_path),
    count_files(0), count_dirs(0), exist_dotdot(false)
{
    try {
        buffer = new struct stat;
        capacity = new vector<string>;
    } catch(std::bad_alloc& er){
        qDebug() << "[Constructor CatalogModel]" << er.what();
    }
    Initialize();
}

CatalogModel::~CatalogModel()
{
    delete buffer;
    delete capacity;
    FreeContainers();
}

void CatalogModel::AllocateContainres()
{
    try {
        buffer = new struct stat;
        file_ = new File*[count_files];
        directory_ = new Directory*[count_dirs];
        dotdot_ = new DotDot*[0];
        dotdot_[0] = new DotDot;
        names_f = new vector<string>;
        names_d = new vector<string>;
        extensions = new vector<string>;
        sizes = new vector<long long>;
        dates = new vector<time_t>;
        container_sizes = new map<long long, string>;
        container_dates = new map<time_t, string>;
        pathes = new vector<string>;
    } catch(std::bad_alloc& er){
        qDebug() << "[AllocateContainers]" << er.what();
    }
}

void CatalogModel::FreeContainers()
{
    if (count_files > 0) {
        for (auto i = 0; i < count_files; ++i){
            delete file_[i];
        }
        delete[] file_;
    }
    if (count_dirs > 0){
        for (auto i = 0; i < count_dirs; ++i){
            delete directory_[i];
        }
        delete[] directory_;
    }
    if (exist_dotdot){
        delete dotdot_[0];
        delete dotdot_;
    }
    delete names_f;
    delete names_d;
    delete extensions;
    delete sizes;
    delete container_sizes;
    delete container_dates;
    if (!pathes->empty()){
        delete pathes;
    }
}

void CatalogModel::SetPath(const char *new_path)
{
    path = new_path;
    qDebug() << "Path: " << new_path;
    Initialize();
}

const char *CatalogModel::GetPath()
{
    return path.c_str();
}

int CatalogModel::GetFileCount()
{
    return count_files;
}

int CatalogModel::GetDirCount()
{
    return count_dirs;
}

bool CatalogModel::CheckDotDot()
{
    return exist_dotdot;
}

void CatalogModel::Reset()
{
    FreeContainers();
    container_files.clear();
    container_dirs.clear();
    capacity->clear();
    count_dirs = 0;
    count_files = 0;
}

string CatalogModel::OpenDir(const std::string &path, const std::string &name)
{
    return (*directory_)->Open(path, name);
}

string CatalogModel::OpenDotDot(const std::string &path)
{
    return (*dotdot_)->Open(path);
}

const char *CatalogModel::GetDotDotName()
{
    return "..";
}

vector<std::string> *CatalogModel::GetFileNames()
{
    names_f->clear();
    for (auto it = container_files.begin(); it != container_files.end(); ++it){
        names_f->push_back((*it)->GetName());
    }
    return names_f;
}

vector<std::string> *CatalogModel::GetDirNames()
{
    names_d->clear();
    for (auto it = container_dirs.begin(); it != container_dirs.end(); ++it){
        names_d->push_back((*it)->GetName());
    }
    return names_d;
}

vector<std::string> *CatalogModel::GetExtensions()
{
    extensions->clear();
    for (auto it = container_files.begin(); it != container_files.end(); ++it){
        extensions->push_back((*it)->GetExtension());
    }
    return extensions;
}

vector<long long> *CatalogModel::GetSizes()
{
    sizes->clear();
    for (auto it = container_files.begin(); it != container_files.end(); ++it){
        sizes->push_back((*it)->GetSize());
    }
    return sizes;
}

vector<time_t> *CatalogModel::GetDates()
{
    // Merging files and directories dates info to
    // one array and return it
    // Using convertation from "time_t" to "string"
    using convert::date::DateConverter;

    try{
        vector<time_t>* dates_d = new vector<time_t>;
        vector<time_t>* dates_f = new vector<time_t>;

        FillDateTime(dates_d, dates_f);
        for (auto it = dates_d->begin(); it != dates_d->end(); ++it){
            dates->push_back(*it);
        }
        for (auto it = dates_f->begin(); it != dates_f->end(); ++it){
            dates->push_back(*it);
        }
        delete dates_d;
        delete dates_f;
    }catch(std::bad_alloc& er){
        qDebug() << "[Constructor GetDates]" << er.what();
    }

    return dates;
}

map<time_t, std::string> *CatalogModel::getContainer_dates() const
{
    return container_dates;
}

vector<std::string>* CatalogModel::GetDirPathes()
{
    pathes->clear();
    for (size_t i = 0; i < container_dirs.size(); ++i){
        pathes->push_back(container_dirs[i]->GetPath());
    }
    return pathes;
}

map<long long, std::string> *CatalogModel::getContainer_sizes() const
{
    return container_sizes;
}

void CatalogModel::Initialize()
{
    // Getting all information about counts of entities
    // then allocatting containers and fill it up
    capacity = GetCapacity(path.c_str());
    FillTypeCounts(path.c_str());
    AllocateContainres();
    SetFileCollection(path.c_str());
    SetDirectoryCollection(path.c_str());
    SetDotDot(path.c_str());
    FillSizeContainer();
    FillDateContainer();
}

void CatalogModel::FillTypeCounts(const string& path)
{
    // Firstly checking for dotdot directory
    // then concatenate a "path" with each element from "capacity"
    // and count a quantity
    string all_path;
    auto it = capacity->begin();
    if(it->compare("..")){
        exist_dotdot = true;
    } else {
        exist_dotdot = false;
    }
    for (; it != capacity->end(); ++it){
        all_path = ConcatPath(path, *it);
        if (IsFile(all_path.c_str())){
            ++count_files;
        } else if(IsDir(all_path.c_str())) {
            ++count_dirs;
        } else {
            qDebug() << "Error! Can't describe entity type!";
        }
    }
}

void CatalogModel::FillDateTime(vector<time_t> *dirs, vector<time_t> *files)
{
    // Fill up dates attribute for entities
    for (auto it = container_dirs.begin(); it != container_dirs.end(); ++it){
        dirs->push_back((*it)->GetDate());
    }
    for (auto it = container_files.begin(); it != container_files.end(); ++it){
        files->push_back((*it)->GetDate());
    }
}

void CatalogModel::FillSizeContainer()
{
    using convert::bit::ConvertToString;

    container_sizes->clear();
    vector<string> size_str = ConvertToString(GetSizes());
    auto it_val = sizes->begin();
    auto it_str = size_str.begin();
    for (; it_str != size_str.end() && it_val != sizes->end();
         ++it_str, ++it_val){
        container_sizes->insert(std::make_pair(*it_val,*it_str));
    }
}

void CatalogModel::FillDateContainer()
{
    using convert::date::ConvertToString;

    container_dates->clear();
    vector<string> date_str = ConvertToString(GetDates());
    auto it_val = dates->begin();
    auto it_str = date_str.begin();
    for (; it_str != date_str.end() && it_val != dates->end();
         ++it_str, ++it_val){
        container_dates->insert(std::make_pair(*it_val,*it_str));
    }
}

void CatalogModel::SetFileCollection(const char *path){
    string all_path;
    int count = 0;
    for (auto it = capacity->begin(); it!= capacity->end(); ++it){
        // concatenate with every entity
        all_path = ConcatPath(path, *it);
        if (IsFile(all_path.c_str())){
            assert(!stat(all_path.c_str(), buffer));
            try{
                file_[count] = new File(
                            BeforeLastSymbol(*it, "."), buffer->st_mtime,all_path,
                            AfterLastSymbol(*it, "."),buffer->st_size);
            } catch (std::bad_alloc& er){
                qDebug() << "Allocation failed! "
                            "Can't create file object!" << er.what();
            }
            container_files.push_back(file_[count]);
            ++count;
        } assert(count <= count_files);
    }
}


void CatalogModel::SetDirectoryCollection(const char *path){
    string all_path;
    int count = 0;
    for (auto it = capacity->begin(); it != capacity->end(); ++it){
        // concatenate with every entity
        all_path = ConcatPath(path, *it);
        if (IsDir(all_path.c_str())){
            assert(!stat(all_path.c_str(), buffer));
            try{
                directory_[count] = new Directory(
                            *it, buffer->st_mtime,all_path);
            } catch (std::bad_alloc& er){
                qDebug() << "Allocation failed!"
                            " Can't create directory object!"
                         << er.what();
            }
            container_dirs.push_back(directory_[count]);
            ++count;
        } assert(count <= count_dirs);
    }

}

void CatalogModel::SetDotDot(const char *path){
    string all_path = static_cast<string>(path) + "/" + "..";
    if (exist_dotdot){
        assert(!stat(all_path.c_str(), buffer));
        try{
            dotdot_[0] = new DotDot("..", buffer->st_mtime,all_path);
        } catch(std::bad_alloc& er){
            qDebug() << "Allocation failed! "
                        "Can't create dotdot object!" << er.what();
        }
    }
}

}
