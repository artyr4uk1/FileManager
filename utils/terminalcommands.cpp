#include "terminalcommands.h"
#include <QDebug>

namespace commands {

namespace unixcommands {

std::string Rename(const char *old_name, const char *new_name)
{
    string com = static_cast<string>("mv ") + old_name + " " + new_name;
    return com;
}

std::string CopyFile(const char *source_path, const char *new_path)
{
    string com = static_cast<string>("cp ") + source_path + " " + new_path;
    return com;
}

std::string MoveFile(const char *source_path, const char *new_path)
{
    string com = static_cast<string>("mv ") + source_path + " " + new_path;
    return com;
}

std::string DeleteFile(const char *file_path)
{
    string com = static_cast<string>("rm ") + file_path;
    return com;
}

std::string CopyDir(const char *source_path, const char *new_path)
{
    string com = static_cast<string>("cp -avr ") + source_path + " " + new_path;
    return com;
}

std::string MoveDir(const char *source_path, const char *new_path)
{
    string com = static_cast<string>("mv ") + source_path + " " + new_path;
    return com;
}

std::string DeleteDir(const char *file_path)
{
    string com = static_cast<string>("rm -r ") + file_path;
    return com;
}

std::string CreateDir(const char *path, const char *name)
{
    string com = static_cast<string>("mkdir ") + path + "/" + name;
    return com;
}

}
}
