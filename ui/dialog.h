#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QProgressDialog>
#include <QWidget>
#include <QPushButton>
#include <QTableWidget>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QCheckBox>

#include <vector>
#include <string>
#include <map>
#include <ctime>

#include "icatalogview.h"
#include "progressrelocate.h"

using std::vector;
using std::string;
using std::map;


class Dialog : public QDialog, public ICatalogView
{
    Q_OBJECT
private:
    enum{ SwitchCount = 1, OperationCount = 4, CatalogCount = 2, AttributesCount = 4, PathSelect = 1};

    string path_left_table;
    string path_right_table;
    QVBoxLayout* layout;
    QGroupBox* catalog_group;
    QGroupBox* operations_group;
    QGroupBox* switch_group;
    QGroupBox* attributes_group;
    QGroupBox* pathset_group; // contain buttons that go to "main" directories
    QTableWidget* table_l;
    QTableWidget* table_r;
    // TODO: Create a Progress obj and pass it to RelocateDialog constructor
    // use obj in thread
    // when thread ends call destructor inside IncreaseStep functions???

    // Triggers to remember a state of atrribure
    bool trigger_ext = true;
    bool trigger_size = true;
    bool trigger_date = true;

    // Current clicked item
    QTableWidgetItem* item_current_name;
    QTableWidgetItem* item_current_ext;
    // Containe a text of entity to operation
    string name_to_operation;

    // Names of sensory actions
    const char* operation_names[OperationCount] = {"Move", "Copy", "Edit", "Delete"};
    const char* attribute_names[AttributesCount] = {"Name", "Ext", "Size", "Date"};
    const char* path_select_names[PathSelect] = {"~"};

    // Containers to store a directry pathes
    vector<string>* pathes;
    string path_item_selected;

    // Widget creating factory methods
    QPushButton* createPathSelectButton(const char* name);
    QPushButton* createOperationButton(const char* name);
    QCheckBox* createCheckBox(const char* name);

    // Draw a column
    void Browse(vector<string>* list, int& counter, QTableWidget *table, int column);
    // Draw a column after "~" button has clicked
    void BrowseSize(vector<long long> *list, map<long long, string>* pairs,
                    int& counter, QTableWidget *table, int column);
    void BrowseDate(vector<time_t>* list, map<time_t, std::string> *pairs,
                    int& counter, QTableWidget *table, int column);
    void BrowseHome(vector<string>* list, int &counter, int column);
    void BrowseSizeHome(vector<long long> *list, map<long long, std::string> *pairs,
                        int &counter, int column);
    void BrowseDateHome(vector<time_t>* list, map<time_t, std::string> *pairs, int &counter, int column);
    // Draw an icon for every cell depends on type of it
    void BrowseNames(vector<string>* list, int& counter, QTableWidget *table, Entity type);
    void BrowseHomeNames(vector<string>* list, int &counter, Entity type);
    // Create columns and equip catalogs
    void InitTables();
    // GroupBox creating on UI
    void createPathSet();
    void createSwitchGroupBox();
    void createCatalogGroupBox();
    void createOperationsGroupBox();
    void createAttributesCheckBox();
    vector<string> ConvertSizes(vector<long long> *sizes);

private slots:
    void LeftTableSorting(int index, Qt::SortOrder order);
    void RightTableSorting(int index, Qt::SortOrder order);
    void OpenDirectoryClicked(int row, int column);
    void PathSelectButtonCliked();
    void OperationButtonCliked();
    void AttributeCheckBoxClicked(int);
    // Trigger for monitoring items and paths for operations
    void ItemClickedSlot(QTableWidgetItem* item);
    // Update catalogs after operations
    static void RefreshWindows(Dialog* obj);
    void UpdateLeftWindow();
    void UpdateRightWindow();

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

    // Draw tables skeloton
    void Form(int files, int dirs);
    // Reset tables skeleton
    void Reset();
    const char *GetPath()const;
    void SetPath(const char *new_path);
    void SetNameList(vector<string>* names_f, vector<string>* names_d);
    void SetExtensionList(vector<string>* extensions, int pos);
    void SetSizeList(vector<long long> *sizes, map<long long, string>* pairs, int pos);
    void SetDateList(vector<time_t> *dates, map<time_t, string> *pairs);
    // Saving an order and column for sorting a data
    void Order();
    void SetPathes(vector<string> *value);
    string GetPathLeftTable() const;
    string GetPathRightTable() const;
    string GetPathItemSelected() const;
signals:
    void Home();
    void OpenDir(const char* dir_name);
    void HideAttribute(int);
    void GetPathes();
    void UpdateLeft(const char*);
    void UpdateRight(const char*);
};

#endif // DIALOG_H
