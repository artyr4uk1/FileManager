#ifndef TABLEWIDGETDATEITEM_H
#define TABLEWIDGETDATEITEM_H

#include <QTableWidgetItem>
#include <vector>
#include <ctime>
#include <string>

// Extends sorting for date column
class TableWidgetDateItem: public QTableWidgetItem
{
private:
    time_t time;
    virtual bool operator<(QTableWidgetItem const &other) const;
public:
    time_t getTime() const;
    void setTime(const time_t &value);
};

#endif // TABLEWIDGETDATEITEM_H
