#ifndef FILEDIRECTORY_H
#define FILEDIRECTORY_H

#include <string>
#include <ctime>

namespace filedirectory {

// Includes and describes file and directory behaviour
using std::string;

enum Icon {FILE, DIRECTORY};

class Entity
{
protected:
    string name;
    time_t date;
    string path;
    Icon type;

public:
    Entity(string name = "NULL", time_t date = 0, string path = "NULL", Icon type = FILE)
        : name(name), date(date), path(path), type(type) {}
    virtual ~Entity() {}

    string GetName();
    time_t GetDate();
    string GetPath();
    Icon GetIcon();

};

class File: public Entity
{
private:
    string extension;
    long size;
public:
    File(string name = "NULL", time_t date = 0, string path = "NULL", string extension = "NULL", int size = 0, Icon type = FILE)
        : Entity(name, date, path, type), extension(extension), size(size)  {}
    ~File() {}

    string GetExtension();
    int GetSize();
};

class Directory: public Entity
{
public:
    Directory(string name = "NULL", time_t date = 0, string path = "NULL", Icon type = DIRECTORY)
        : Entity(name, date, path, type) {}
    ~Directory() {}

    virtual string Open(const string& path, const string& name);
};

class DotDot: public Directory
{
public:
    DotDot(string name = "NULL", time_t date = 0, string path = "NULL", Icon type = DIRECTORY)
        : Directory(name, date, path, type) {}
    ~DotDot () {}

    string Open(const string &path);
};
}
#endif // FILEDIRECTORY_H
