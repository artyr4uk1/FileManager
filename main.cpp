#include <QApplication>
#include "ui/dialog.h"
#include "catalogpresenter.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Dialog* view = new Dialog();
    CatalogPresenter* presenter = new CatalogPresenter(view);

    Q_UNUSED(presenter);
    view->show();
    return a.exec();
}
