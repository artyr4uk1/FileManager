#ifndef TERMINALCOMMANDS_H
#define TERMINALCOMMANDS_H

#include <string>

namespace commands {

// Containes termminal commands for OS
using std::string;

namespace unixcommands {

string CreateDir(const char* path, const char* name);
string Rename(const char* old_name, const char* new_name);
string CopyFile(const char* source_path, const char* new_path);
string MoveFile(const char* source_path, const char* new_path);
string DeleteFile(const char* file_path);

string CopyDir(const char* source_path, const char* new_path);
string MoveDir(const char* source_path, const char* new_path);
string DeleteDir(const char* file_path);

}
namespace windowscommands {

}


}
#endif // TERMINALCOMMANDS_H
