#ifndef EDITDIALOG_H
#define EDITDIALOG_H

#include <QDialog>
#include <QPushButton>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLineEdit>

#include <string>

using std::string;

class EditDialog: public QDialog
{
    Q_OBJECT
private:
    QLineEdit* name_edit;
    string global_path;
    string table_path;
    string name_to_operation;

    // Compose a new path with edited name
    string MakeNewPath(const char* new_name, const char* old_path);
private slots:
    void EditEntity();
signals:
    void CloseWindow();

public:
    EditDialog(const string& table_path,const std::string &name,QWidget* parent = 0);
    ~EditDialog() {}

    QString GetNewName();
    void setGlobal_path(const char *value);
    std::string getGlobal_path() const;
};
#endif // EDITDIALOG_H
