#include "utils/convert.h"

#include <cmath>
#include <string.h>
#include <ctime>
#include <cstdio>
#include <sstream>
#include <iomanip>

namespace convert {

namespace date {

string DateConverter(time_t time)
{
    char date[36];
    strftime(date, 36, "%d.%m.%Y %H:%M:%S", localtime(&time));
    return date;
}

vector<std::string> ConvertToString(vector<time_t> *sizes)
{
    vector<string> temp;
    for (auto it = sizes->begin(); it != sizes->end(); ++it){
        temp.push_back(DateConverter(*it));
    }
    return temp;
}

}

namespace bit {

void BitConverter(long long bits, Size **variable){
    if (bits > kGigabyte){
        FillStruct(variable, bits, kGigabyte, tGigabyte);
    } else if (bits > kMegabyte){
        FillStruct(variable, bits, kMegabyte, tMegabyte);
    } else if (bits > kKilobyte){
        FillStruct(variable, bits, kKilobyte, tKilobyte);
    } else{
        FillStruct(variable, bits, kByte, tByte);
    }
}

void FillStruct(Size** container, long long bits, const int kDim, const char* dim){
    // Convert a value by dividing it
    // and store information to struct
    long double val = static_cast<double>(bits)/kDim;
    (*container)->number = val;
    strcpy((*container)->dimension, dim);
}

vector<std::string> ConvertToString(vector<long long> *sizes)
{
    // Converting size of files to comfort look:
    // cutting a precision of sizes and
    // store values to array
    using std::stringstream;

    Size* container = new Size;
    vector<string> temp;
    string mix;
    stringstream stream;
    for (auto it = sizes->begin(); it != sizes->end(); ++it){
        BitConverter(*it,&container);
        stream << std::fixed << std::setprecision(2) << container->number;
        mix = stream.str();
        mix = mix + " " + container->dimension;
        temp.push_back(mix);
        stream.str(std::string());
        stream.clear();
    }
    return temp;
}

}
}
