#include "ui/relocatedialog.h"
#include "ui/deletedialog.h"
#include "utils/pulldata.h"
#include "utils/terminalcommands.h"
#include "progressrelocate.h"
#include "utils/relocating.h"

#include <thread>

void RelocateDialog::SetGlobalPath(const char *value)
{
    global_path = value;
}

string RelocateDialog::GetGlobalPath() const
{
    return global_path;
}

void RelocateDialog::FillPathBox(const vector<std::string>* pathes)
{
    using pulldata::AfterLastSymbol;
    using pulldata::BeforeLastSymbol;

    for (auto it = pathes->begin(); it != pathes->end(); ++it){
        string str = AfterLastSymbol(*it, "/");
        if (str == BeforeLastSymbol(name_to_operation, ".") || str == "..")
            continue;
        path_box->addItem(QString::fromStdString(*it));
    }
}

void RelocateDialog::RelocateEntity()
{
    using pulldata::IsDir;
    using pulldata::IsFile;
    using pulldata::VerifyBackspace;
    using pulldata::VerifyToDir;
    using relocating::RelocateDir;
    using relocating::RelocateFile;
    using commands::unixcommands::CopyDir;
    using commands::unixcommands::CopyFile;
    using commands::unixcommands::MoveDir;
    using commands::unixcommands::MoveFile;

    if (path_box->count() == 0)
        emit CloseWindow();
    // Verify path to relocate and source path

    string current_path = global_path + "/" + VerifyToDir(name_to_operation);
    string destination = PathToRelocate().toStdString().c_str();
    string after_path = destination + "/" + VerifyToDir(name_to_operation);

    if (IsDir(current_path.c_str())){
        if (operation == 0){
            // Copy
            std::thread relocate_thread(RelocateDir,
                                        current_path.c_str(), after_path.c_str(), 0);
            relocate_thread.detach();
        } else {
            // Move
            std::thread relocate_thread(RelocateDir,
                                        current_path.c_str(), after_path.c_str(), 1);
            relocate_thread.detach();
        }
    } else {
        if (operation == 0){
            // Copy
            std::thread relocate_thread(RelocateFile,
                                        current_path.c_str(), after_path.c_str(), 0);
            relocate_thread.detach();
        } else {
            // Move
            std::thread relocate_thread(RelocateFile,
                                        current_path.c_str(), after_path.c_str(), 1);
            relocate_thread.detach();
        }
    }
    emit CloseWindow();
}

RelocateDialog::RelocateDialog(const std::string &table_path, vector<std::string> *pathes,
                               const string& name, bool operation, QWidget *parent) :
    table_path(table_path),name_to_operation(name),operation(operation)
{
    using pulldata::BeforeLastSymbol;

    this->setFixedSize(300, 100);
    path_box = new QComboBox;

    QLabel* hint = new QLabel("Choose relocate path:");
    QPushButton* ok_button = new QPushButton("OK");
    QPushButton* cancel_button = new QPushButton("Cancel");

    QObject::connect(ok_button, SIGNAL(clicked()), SLOT(RelocateEntity()));
    QObject::connect(this, SIGNAL(CloseWindow()),  SLOT(accept()));
    QObject::connect(cancel_button, SIGNAL(clicked()), SLOT(reject()));

    QVBoxLayout* layout_v = new QVBoxLayout;
    QHBoxLayout* layout_h = new QHBoxLayout;

    layout_v->addWidget(hint);
    layout_v->addWidget(path_box);
    layout_v->addLayout(layout_h);

    layout_h->addWidget(ok_button);
    layout_h->addWidget(cancel_button);
    setLayout(layout_v);

    FillPathBox(pathes);
    SetGlobalPath(BeforeLastSymbol(pathes->begin()->c_str(), "/").c_str());

}

RelocateDialog::~RelocateDialog()
{
}

QString RelocateDialog::PathToRelocate()
{
    return path_box->currentText();
}
