#ifndef DELETEDIALOG_H
#define DELETEDIALOG_H

#include <QDialog>
#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>

#include <string>

using std::string;

class DeleteDialog: public QDialog
{
    Q_OBJECT
private:
    QLabel* question;
    string table_path;
    string name_to_operation;

private slots:
    void DeleteEntity();
signals:
    void CloseWindow();
public:
    DeleteDialog(const string& table_path,const std::string &name,QWidget* parent = 0);
    ~DeleteDialog() {}
};
#endif // DELETEDIALOG_H
