#include "relocating.h"
#include "convert.h"
#include "terminalcommands.h"
#include "pulldata.h"
#include "ui/progressrelocate.h"

#include <string>
#include <sys/stat.h>  // Unix
#include <dirent.h>  // Unix
#include <QDebug>


namespace relocating {

using std::ios;
void RelocateFile(const char *src_path, const char *dest_path, int relocate_type/*=0*/)
{
    using commands::unixcommands::DeleteFile;
    using pulldata::VerifyBackspace;

    ifstream source(src_path, ios::binary);
    ofstream dest(dest_path, ios::binary);
    if (!source.is_open() || !dest.is_open()){
        qDebug() << "Fail to open file!";
        return;
    }
    source.seekg(0, ios::end);
    // get all size of file
    ifstream::pos_type all_size = source.tellg();
    source.seekg(0);
    // get the value to increase a progress
    int progress = GetBufferSize(all_size);
    ifstream::pos_type copied = 0;

    Progress* progress_bar = new Progress();
    progress_bar->SetMaxValue(all_size);
    progress_bar->show();

    while (copied < all_size) {
        char* buffer = new char[progress];
        // write a correct number of bites in the end
        if (copied + progress > all_size){
            progress -= (copied + progress) - all_size;
            delete[] buffer;
            buffer = new char[progress];
        }

        source.read(buffer, progress);
        dest.write(buffer, progress);

        copied += progress;
        // Make a progress
        progress_bar->IncreaseStep(copied);

        qDebug() << copied;
        delete[] buffer;
    }
    if (relocate_type == 1)
        // move = copy + delete source
        system(DeleteFile(VerifyBackspace(src_path).c_str()).c_str());

    source.close();
    dest.close();
}

ifstream::pos_type GetBufferSize(const ifstream::pos_type size)
{
    if (size < convert::bit::Kilo){
        return size;
    } else {
        return convert::bit::Kilo;
    }
}

void RelocateDir(const char *src_path, const char *dest_path, int relocate_type/*=0*/)
{
    using commands::unixcommands::CreateDir;
    using commands::unixcommands::DeleteDir;
    using pulldata::VerifyBackspace;
    using pulldata::AfterLastSymbol;
    using pulldata::IsDir;

    struct dirent* entity = new struct dirent;
    struct stat buffer;
    DIR* disk = opendir(src_path);
    if (disk == NULL){
        qDebug() << "Error! Can't open directory! Path:" << src_path;
        exit(EXIT_FAILURE);
    }
    system(CreateDir(VerifyBackspace(dest_path).c_str(),"").c_str());
    while ((entity = readdir(disk))){
        if ((*entity->d_name == '.') ||
                (*entity->d_name == '..'))
            continue;
        std::string src_str = src_path;
        std::string dest_str = dest_path;
        std::string entity_path = src_str + "/" + entity->d_name;
        if  (!IsDir(entity_path.c_str())){
            dest_str = dest_str + "/" + entity->d_name;
            RelocateFile(entity_path.c_str(),dest_str.c_str(),relocate_type);
        } else if (IsDir(entity_path.c_str())){
            dest_str = dest_str + "/" + entity->d_name;
            RelocateDir(entity_path.c_str(),dest_str.c_str(),relocate_type);
        }
    }
    if (relocate_type)
        system(DeleteDir(VerifyBackspace(src_path).c_str()).c_str());
    delete entity;
}


}
