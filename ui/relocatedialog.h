#ifndef RELOCATEDIALOG_H
#define RELOCATEDIALOG_H

#include <QDialog>
#include <QComboBox>
#include <QPushButton>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QTableWidgetItem>

#include <string>
#include <vector>

using std::string;
using std::vector;

class RelocateDialog: public QDialog
{
    Q_OBJECT
private:
    QComboBox* path_box;
    // Containes a path of catalog where entity from
    string global_path;
    string table_path;
    // Etity name to operation
    string name_to_operation;

    bool operation; // 0 - copy, 1 - move
    // Containe pathes for check box
    void FillPathBox(const vector<std::string>* pathes);
    void PathAction();

private slots:
    void RelocateEntity();
signals:
    void CloseWindow();

public:
    RelocateDialog(const string& table_path, vector<string>* pathes,
                   const std::string &name, bool operation=0, QWidget* parent=0);
    ~RelocateDialog();

    QString PathToRelocate();
    void SetGlobalPath(const char *value);
    std::string GetGlobalPath() const;
};
#endif // RELOCATEDIALOG_H
