#ifndef CONVERT_H
#define CONVERT_H

#include <string>
#include <ctime>
#include <sstream>
#include <vector>


namespace convert {

// Converting auxiliary functions
using std::string;

namespace date {

// Declaration of function to convert datetime
using std::stringstream;
using std::vector;

string DateConverter(time_t time);
vector<string> ConvertToString(vector<time_t> *sizes);
}

namespace bit {

namespace {

// Constants of dimensions
const long Tera = 137438953472;
const int Giga = 134217728;
const int Mega = 131072;
const int Kilo = 128;
const int Byte = 1;

const int kByte = 1;
const int kKilobyte = 1000;
const int kMegabyte = 1000000;
const int kGigabyte = 1000000000;

const char* tBit = "Bit";
const char* tByte = "B";
const char* tKilobyte = "Kb";
const char* tMegabyte = "Mb";
const char* tGigabyte = "Gb";
const char* tTerabyte = "Tb";
}


using std::vector;
// Declaration of function to convert a size of files
struct Size
{
    double number;
    char dimension[3];
};

// Convert a bit value
// TODO: fix bug with conversion size
void BitConverter(long long bits, Size** variable);
void FillStruct(Size** container, long long bits, const int kDim, const char* dim);
vector<string> ConvertToString(vector<long long> *sizes);
}

}
#endif // CONVERT_H
