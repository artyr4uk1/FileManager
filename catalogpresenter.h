#ifndef CATALOGPRESENTER_H
#define CATALOGPRESENTER_H

#include <QObject>
#include <string>
#include <vector>

#include "catalogmodel.h"

namespace {
     const char* home_path_linux = "/home";
}

using std::vector;
using std::string;

using catalogmodel::CatalogModel;

class ICatalogView;

class CatalogPresenter: public QObject
{
    Q_OBJECT
private:
    CatalogModel* model;
    ICatalogView* view;

    void UpdateView() const;

private slots:
    void HomeAction();
    void OpenDirAction(const char* name);
    void SendPathes();
    void Update(const char*path);

public:
    explicit CatalogPresenter(ICatalogView* view, QObject *parent = 0);
    ~CatalogPresenter() {}
};

#endif // CATALOGPRESENTER_H
