#ifndef RELOCATING_H
#define RELOCATING_H

#include <iostream>
#include <fstream>
#include <ctime>

#include "utils/convert.h"

namespace relocating {
using std::ifstream;
using std::ofstream;

// Getting a size of progress relocating
ifstream::pos_type GetBufferSize(const ifstream::pos_type size);
// Relocate functions
// relocate_type = 0 - copy
// relocate_type = 1 - move
void RelocateFile(const char* src_path, const char* dest_path, int relocate_type = 0);
void RelocateDir(const char* src_path, const char* dest_path, int relocate_type = 0);
}

#endif // RELOCATING_H
