#include "tablewidgetdateitem.h"

time_t TableWidgetDateItem::getTime() const
{
    return time;
}

void TableWidgetDateItem::setTime(const time_t &value)
{
    time = value;
}

bool TableWidgetDateItem::operator<(const QTableWidgetItem &other) const
{
    const TableWidgetDateItem* item = dynamic_cast<const TableWidgetDateItem*>(&other);
    double dif = std::difftime(item->getTime(), getTime());
    if (dif > 0){
        return true;
    } else
        return false;
}
