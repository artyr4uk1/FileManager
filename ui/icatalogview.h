#ifndef ICATALOGVIEW_H
#define ICATALOGVIEW_H

#include "catalogmodel.h"

#include <vector>

// View interface
using std::vector;
using std::string;

class ICatalogView
{
public:
    virtual void Form(int files, int dirs)=0;
    virtual void Reset()=0;
    virtual const char* GetPath()const=0;
    virtual void SetPath(const char* new_path)=0;
    virtual void SetNameList(vector<string>* names_f, vector<string>* names_d)=0;
    virtual void SetExtensionList(vector<string>* extensions, int pos)=0;
    virtual void SetSizeList(vector<long long> *sizes, std::map<long long, string>* pairs, int pos)=0;
    virtual void SetDateList(vector<time_t> *dates, std::map<time_t, string> *pairs)=0;
    virtual void Order()=0;
    virtual void SetPathes(vector<string> *value)=0;
public:
    // signals
    virtual void Home()=0;
    virtual void OpenDir(const char* dir_name)=0;
    virtual void HideAttribute(int)=0;
    virtual void GetPathes()=0;
    virtual void UpdateLeft(const char*)=0;
    virtual void UpdateRight(const char*)=0;
protected:
    enum class Entity {FILE, DIR};
};
#endif // ICATALOGVIEW_H
