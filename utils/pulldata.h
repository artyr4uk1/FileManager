#ifndef PULLDATA_H
#define PULLDATA_H

#include <vector>
#include <string>

namespace pulldata {

// Functions declartation to pull information about files and directories
using std::vector;
using std::string;

string ConcatPath(string path, string name);
vector<std::string> *GetCapacity(const char* path);

long long GetSize(const char* path);
int GetDirCapacity(const char* path);

bool IsFile(const char* path);
bool IsDir(const char* path);

// Separate strings
string AfterLastSymbol(const std::string &name, const char* symbol);
std::string BeforeLastSymbol(const std::string &name, const char *symbol);

// Convert unix paths
string VerifyToDir(const string& name);
string VerifyBackspace(string name);
}
#endif // PULLCONFIG_UNIX_H
