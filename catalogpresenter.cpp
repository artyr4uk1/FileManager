#include "catalogpresenter.h"
#include "ui/icatalogview.h"
#include <QDebug>

using catalogmodel::CatalogModel;

CatalogPresenter::CatalogPresenter(ICatalogView *view, QObject *parent)
    : QObject(parent), model(new CatalogModel(home_path_linux)), view(view)
{
    QObject* view_obj = dynamic_cast<QObject*>(view);

    QObject::connect(view_obj, SIGNAL(OpenDir(const char*)),
                     this, SLOT(OpenDirAction(const char*)));
    QObject::connect(view_obj, SIGNAL(Home()),
                     this, SLOT(HomeAction()));
    QObject::connect(view_obj, SIGNAL(GetPathes()),
                     this, SLOT(SendPathes()));
    QObject::connect(view_obj, SIGNAL(UpdateLeft(const char*)),
                                       this, SLOT(Update(const char*)));
    QObject::connect(view_obj, SIGNAL(UpdateRight(const char*)),
                                       this, SLOT(Update(const char*)));
    view->SetPath(home_path_linux);
    UpdateView();
}

void CatalogPresenter::UpdateView() const
{
    view->Reset();
    view->Form(model->GetFileCount(), model->GetDirCount());
    view->SetNameList(model->GetFileNames(), model->GetDirNames());
    view->SetExtensionList(model->GetExtensions(), model->GetDirCount());
    view->SetSizeList(model->GetSizes(),model->getContainer_sizes(),model->GetDirCount());
    view->SetDateList(model->GetDates(),model->getContainer_dates());
    view->Order();
}

void CatalogPresenter::HomeAction()
{
    model->Reset();
    model->SetPath(home_path_linux);
    view->SetPath(home_path_linux);
    UpdateView();
}

void CatalogPresenter::OpenDirAction(const char* name)
{
    // Calling an Open functions
    // respectively to entity
    string path = view->GetPath();
    string dir_name = name;
    string next_path;
    if (!strcmp(name, "..")){
        next_path = model->OpenDotDot(path);
    } else {
        next_path = model->OpenDir(path,dir_name);
    }
    if (next_path == "")
        return;
    model->Reset();
    model->SetPath(next_path.c_str());
    view->SetPath(next_path.c_str());
    UpdateView();
}

void CatalogPresenter::SendPathes()
{
    view->SetPathes(model->GetDirPathes());
}

void CatalogPresenter::Update(const char *path)
{
    model->Reset();
    model->SetPath(path);
    view->SetPath(path);
    UpdateView();
}
