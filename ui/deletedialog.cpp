#include "ui/deletedialog.h"
#include "utils/pulldata.h"
#include "utils/terminalcommands.h"

void DeleteDialog::DeleteEntity()
{
    using pulldata::VerifyBackspace;
    using pulldata::VerifyToDir;
    using pulldata::IsDir;
    using pulldata::IsFile;
    using commands::unixcommands::DeleteFile;
    using commands::unixcommands::DeleteDir;

    // Verify path to relocate and source path
    string current_path = table_path + "/" + VerifyToDir(name_to_operation);
    // Delete respectively to entity
    if (IsDir(current_path.c_str())){
        current_path = VerifyBackspace(current_path);
        system(DeleteDir(current_path.c_str()).c_str());
    } else if (IsFile(current_path.c_str())){
        system(DeleteFile(current_path.c_str()).c_str());
    }
    emit CloseWindow();
}

DeleteDialog::DeleteDialog(const string &table_path, const std::string &name, QWidget *parent) :
    table_path(table_path), name_to_operation(name)
{
    this->setFixedSize(300, 100);

    question = new QLabel("Do you really want to delete?");

    QPushButton* ok_button = new QPushButton("OK");
    QPushButton* cancel_button = new QPushButton("Cancel");

    QObject::connect(ok_button, SIGNAL(clicked()), SLOT(DeleteEntity()));
    QObject::connect(this, SIGNAL(CloseWindow()),  SLOT(accept()));
    QObject::connect(cancel_button, SIGNAL(clicked()), SLOT(reject()));

    QVBoxLayout* layout_v = new QVBoxLayout;
    QHBoxLayout* layout_h = new QHBoxLayout;

    layout_v->addWidget(question);
    layout_v->addLayout(layout_h);
    layout_h->addWidget(ok_button);
    layout_h->addWidget(cancel_button);
    setLayout(layout_v);
}
