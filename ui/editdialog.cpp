#include "ui/editdialog.h"
#include "utils/pulldata.h"
#include "utils/terminalcommands.h"

EditDialog::EditDialog(const string &table_path, const std::string &name, QWidget *parent) :
    table_path(table_path), name_to_operation(name)
{
    this->setFixedSize(300, 100);

    name_edit = new QLineEdit;
    QLabel* hint = new QLabel("Set a new name:");
    QPushButton* ok_button = new QPushButton("OK");
    QPushButton* cancel_button = new QPushButton("Cancel");

    QObject::connect(ok_button, SIGNAL(clicked()), SLOT(EditEntity()));
    QObject::connect(this, SIGNAL(CloseWindow()),  SLOT(accept()));
    QObject::connect(cancel_button, SIGNAL(clicked()), SLOT(reject()));

    QVBoxLayout* layout_v = new QVBoxLayout;
    QHBoxLayout* layout_h = new QHBoxLayout;

    layout_v->addWidget(hint);
    layout_v->addWidget(name_edit);
    layout_v->addLayout(layout_h);
    layout_h->addWidget(ok_button);
    layout_h->addWidget(cancel_button);
    setLayout(layout_v);

}

void EditDialog::EditEntity()
{
    using pulldata::IsDir;
    using pulldata::IsFile;
    using pulldata::VerifyToDir;
    using pulldata::VerifyBackspace;
    using commands::unixcommands::Rename;

    if (name_edit->text().toStdString().c_str() == ""){
        emit CloseWindow();
    }
    // Verify path to relocate and source path
    string current_path = table_path + "/" + VerifyToDir(name_to_operation);
    current_path = VerifyBackspace(current_path);
    string destination = MakeNewPath(VerifyBackspace(GetNewName().toStdString().c_str()).c_str(),
                            current_path.c_str()).c_str();
    system(Rename(current_path.c_str(), destination.c_str()).c_str());
    emit CloseWindow();
}

QString EditDialog::GetNewName()
{
    return name_edit->text();
}

void EditDialog::setGlobal_path(const char *value)
{
    global_path = value;
}

string EditDialog::getGlobal_path() const
{
    return global_path;
}


std::string EditDialog::MakeNewPath(const char *new_name, const char *old_path)
{
    // Create a path by composing a new name from lineEdit
    // and all separate symbols
    string path = old_path;
    string global_path = "";
    string ext = "";
    size_t pos_ext = path.find_last_of(".");
    size_t pos_slash = path.find_last_of("/");
    if (pos_slash != string::npos){
        global_path = path.substr(0,pos_slash+1);
    } else {
        exit(EXIT_FAILURE);
    }
    if (pos_ext != string::npos)
        ext = path.substr(pos_ext);
    string ret_path = global_path + new_name + ext;
    return ret_path;
}

