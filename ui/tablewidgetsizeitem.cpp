#include "tablewidgetsizeitem.h"

long long TableWidgetSizeItem::getSize() const
{
    return size;
}

void TableWidgetSizeItem::setSize(long long value)
{
    size = value;
}

bool TableWidgetSizeItem::operator<(const QTableWidgetItem &other) const
{
    const TableWidgetSizeItem* item = dynamic_cast<const TableWidgetSizeItem*>(&other);
    bool ret = false;
    ret  = getSize() < item->getSize();
    return ret;
}
