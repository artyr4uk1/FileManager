#include "ui/dialog.h"
#include "ui/relocatedialog.h"
#include "ui/deletedialog.h"
#include "ui/editdialog.h"
#include "utils/convert.h"
#include "utils/pulldata.h"
#include "tablewidgetdateitem.h"
#include "tablewidgetsizeitem.h"
#include "utils/relocating.h"

#include <iomanip>
#include <QDebug>
#include <QHeaderView>
#include <QMouseEvent>
#include <QEvent>

void Dialog::createCatalogGroupBox()
{
    catalog_group = new QGroupBox;
    QHBoxLayout* layout = new QHBoxLayout;

    table_l = new QTableWidget;
    table_r = new QTableWidget;
    table_l->setSelectionMode(QAbstractItemView::SingleSelection);
    table_r->setSelectionMode(QAbstractItemView::SingleSelection);
    table_l->setSelectionBehavior(QAbstractItemView::SelectRows);
    table_r->setSelectionBehavior(QAbstractItemView::SelectRows);

    table_l->setShowGrid(false);
    table_r->setShowGrid(false);
    // Enable a sort indicator on columns
    table_l->horizontalHeader()->setSortIndicatorShown(true);
    table_r->horizontalHeader()->setSortIndicatorShown(true);
    QObject::connect(table_l, SIGNAL(cellDoubleClicked(int,int)),
                     this, SLOT(OpenDirectoryClicked(int,int)));
    QObject::connect(table_r, SIGNAL(cellDoubleClicked(int,int)),
                     this, SLOT(OpenDirectoryClicked(int, int)));
    // Monitoring names available to operation and tables_paths
    // by reading it when click an item
    QObject::connect(table_l, SIGNAL(itemClicked(QTableWidgetItem*)),
                     this, SLOT(ItemClickedSlot(QTableWidgetItem*)));
    QObject::connect(table_r, SIGNAL(itemClicked(QTableWidgetItem*)),
                     this, SLOT(ItemClickedSlot(QTableWidgetItem*)));

    // Artifical signal to update catalogs
    QObject::connect(table_l, SIGNAL(viewportEntered()),
                      this, SLOT(UpdateLeftWindow()));
    QObject::connect(table_r, SIGNAL(viewportEntered()),
                      this, SLOT(UpdateRightWindow()));
    QHeaderView *header_l = qobject_cast<QTableView *>(table_l)->horizontalHeader();
    QHeaderView *header_r = qobject_cast<QTableView *>(table_r)->horizontalHeader();
    // Change sort indicators
    QObject::connect(header_l, SIGNAL(sortIndicatorChanged(int,Qt::SortOrder)),
                     this, SLOT(LeftTableSorting(int,Qt::SortOrder)));
    QObject::connect(header_r, SIGNAL(sortIndicatorChanged(int,Qt::SortOrder)),
                     this, SLOT(RightTableSorting(int,Qt::SortOrder)));
    layout->addWidget(table_l);
    layout->addWidget(table_r);
    catalog_group->setLayout(layout);
}

void Dialog::createOperationsGroupBox()
{
    operations_group = new QGroupBox;
    QHBoxLayout* layout = new QHBoxLayout;

    for (int i = 0; i < OperationCount; i++){
        layout->addWidget(createOperationButton(operation_names[i]));
    }
    operations_group->setLayout(layout);
}

void Dialog::createAttributesCheckBox()
{
    attributes_group = new QGroupBox("Show/Hide");
    attributes_group->setStyleSheet("font-size: 13px; padding-top: 5px; border: 0;");

    QHBoxLayout* layout = new QHBoxLayout;
    layout->setAlignment(Qt::AlignLeft);

    for (int i = 1; i < AttributesCount; i++){
        layout->addWidget(createCheckBox(attribute_names[i]));
    }
    attributes_group->setLayout(layout);
}

void Dialog::LeftTableSorting(int index, Qt::SortOrder order)
{
    table_l->sortByColumn(index, order);
}

void Dialog::RightTableSorting(int index, Qt::SortOrder order)
{
    table_r->sortByColumn(index, order);
}

void Dialog::OpenDirectoryClicked(int row, int column)
{
    // Open directories only by firs column text
    if (column == 0){
        QTableWidget* table = qobject_cast<QTableWidget*>(sender());
        QTableWidgetItem* item = table->item(row,column);
        string cell_text = item->text().toUtf8().constData();
        item = table->item(row,column+1);
        // If item not a directory don't open it
        if (!(item->text() != "<dir>"))
            emit OpenDir(cell_text.c_str());
    }
}

void Dialog::PathSelectButtonCliked()
{
    emit Home();
}

void Dialog::SetPathes(vector<string> *value)
{
    pathes = value;
}

string Dialog::GetPathLeftTable() const
{
    return path_left_table;
}

string Dialog::GetPathRightTable() const
{
    return path_right_table;
}

string Dialog::GetPathItemSelected() const
{
    return path_item_selected;
}

QPushButton *Dialog::createPathSelectButton(const char *name)
{
    QPushButton* btn = new QPushButton(name);
    btn->setFont(QFont("Consolas", 10, QFont::Cursive));
    connect(btn, SIGNAL(clicked(bool)), this, SLOT(PathSelectButtonCliked()));
    return btn;
}

QPushButton *Dialog::createOperationButton(const char *name)
{
    QPushButton *btn = new QPushButton(name);
    btn->setFont(QFont("Consolas", 10, QFont::Cursive));
    connect(btn, SIGNAL(clicked(bool)), this, SLOT(OperationButtonCliked()));
    return btn;
}

QCheckBox *Dialog::createCheckBox(const char *name)
{
    QCheckBox* checkbox = new QCheckBox(name);
    checkbox->setChecked(true);
    connect(checkbox, SIGNAL(stateChanged(int)), this, SLOT(AttributeCheckBoxClicked(int)));
    return checkbox;
}

void Dialog::InitTables()
{
    table_l->setRowCount(0);
    table_r->setRowCount(0);
    table_l->setColumnCount(AttributesCount);
    table_r->setColumnCount(AttributesCount);

    QStringList columns;
    for (int i = 0; i < AttributesCount; ++i){
        columns.push_back(attribute_names[i]);
    }
    table_l->setHorizontalHeaderLabels(columns);
    table_r->setHorizontalHeaderLabels(columns);
    operations_group->setDisabled(true);
}

void Dialog::createPathSet()
{
    pathset_group = new QGroupBox;
    QHBoxLayout* layout = new QHBoxLayout;
    layout->setAlignment(Qt::AlignLeft);
    for (int i = 0; i < PathSelect; i++){
        layout->addWidget(createPathSelectButton(path_select_names[i]));
    }
    pathset_group->setLayout(layout);
}

void Dialog::OperationButtonCliked()
{
    using pulldata::AfterLastSymbol;

    if (table_l->currentRow() != -1 || table_r->currentRow() != -1){
        QPushButton* button = qobject_cast<QPushButton*>(sender());
        if (button->text() == "Delete"){
            DeleteDialog* delete_window = new DeleteDialog(GetPathItemSelected(),name_to_operation);
            delete_window->exec();
            delete delete_window;
        } else if (button->text() == "Edit"){
            EditDialog* edit_window = new EditDialog(GetPathItemSelected(),name_to_operation);
            edit_window->exec();
            delete edit_window;
        } else if (button->text() == "Copy"){
            emit GetPathes(); // fill up catalog pathes for combobox
            RelocateDialog* relocate_window =
                    new RelocateDialog(GetPathItemSelected(),pathes,name_to_operation);
            relocate_window->exec();
            delete relocate_window;
        } else if(button->text() == "Move"){
            emit GetPathes(); // fill up catalog pathes for combobox
            RelocateDialog* relocate_window =
                    new RelocateDialog(GetPathItemSelected(),pathes,name_to_operation, 1);
            relocate_window->exec();
            delete relocate_window;
        }

//          table_l->viewportEntered();
//          table_r->viewportEntered();
    }
}

void Dialog::AttributeCheckBoxClicked(int)
{
    QCheckBox* attribite = qobject_cast<QCheckBox*>(sender());

    if (attribite->text() == "Ext") {
        //  Hide size attribute
        table_l->setColumnHidden(1,trigger_ext);
        table_r->setColumnHidden(1,trigger_ext);
        trigger_ext = !trigger_ext;
    } else if (attribite->text() == "Size") {
        //  Hide extension attribute
        table_l->setColumnHidden(2,trigger_size);
        table_r->setColumnHidden(2,trigger_size);
        trigger_size = !trigger_size;
    } else {
        //  Hide date attribute
        table_l->setColumnHidden(3,trigger_date);
        table_r->setColumnHidden(3,trigger_date);
        trigger_date = !trigger_date;
    }
}

void Dialog::ItemClickedSlot(QTableWidgetItem *item)
{
    // Getting current item information
    // monitoring a paths of catalogs
    if (item->column() == 0){
        operations_group->setDisabled(false);
        item_current_name = item;
        int row = item_current_name->row();
        int column = item_current_name->column();
        QTableWidgetItem* ext = 0;
        if (QObject::sender() == table_l){
            path_item_selected = (GetPathLeftTable());
            ext = table_l->item(row, column+1);
        } else if (QObject::sender() == table_r){
            path_item_selected = (GetPathRightTable());
            ext = table_r->item(row, column+1);
        }
        name_to_operation = item->text().toStdString() +
                "." + ext->text().toStdString();
    }
}

void Dialog::RefreshWindows(Dialog *obj)
{
    obj->UpdateLeftWindow();
    obj->UpdateRightWindow();
}

void Dialog::UpdateLeftWindow()
{
    emit UpdateLeft(path_left_table.c_str());
}

void Dialog::UpdateRightWindow()
{
    emit UpdateRight(path_right_table.c_str());
}

Dialog::Dialog(QWidget *parent)
    : QDialog(parent)
{
    this->setStyleSheet("background: rgb(232,229,160);");
    this->setWindowFlags(Qt::WindowMaximized & Qt::WindowFullscreenButtonHint & Qt::WindowMaximizeButtonHint);
    this->resize(1010, 650);
    createAttributesCheckBox();
    createCatalogGroupBox();
    createOperationsGroupBox();
    createPathSet();

    layout = new QVBoxLayout;
    layout->setContentsMargins(5, 5, 5, 5);
    layout->addWidget(attributes_group);
    layout->addWidget(pathset_group);
    layout->addWidget(catalog_group);
    layout->addWidget(operations_group);

    this->setLayout(layout);
    this->setWindowTitle("File Manager");

    InitTables();
    this->table_l->installEventFilter(this);
    this->table_r->installEventFilter(this);
}

Dialog::~Dialog()
{
}

void Dialog::Browse(vector<string> *list, int &counter, QTableWidget *table, int column)
{
    QTableWidgetItem* item = 0;
    for (auto it = list->begin(); it != list->end(); ++it){
        QString text = QString::fromStdString(*it);
        item = new QTableWidgetItem(text);
        item->setFlags(item->flags() & ~Qt::ItemIsEditable);
        item->setTextAlignment(Qt::AlignRight);
        table->setItem(counter, column, item);
        counter++;
    }
}

void Dialog::BrowseSize(vector<long long> *list, map<long long, string>* pairs,
                        int &counter, QTableWidget *table, int column)
{
    // Fill up a column by user class
    TableWidgetSizeItem* item = 0;
    for (auto it = list->begin(); it != list->end(); ++it){
        map<long long, string>::iterator pair_str = pairs->find(*it);
        QString text = QString::fromStdString(pair_str->second);

        item = new TableWidgetSizeItem;
        // set time value for sorting items
        item->setSize(*it);
        item->setText(text);
        item->setFlags(item->flags() & ~Qt::ItemIsEditable);
        item->setTextAlignment(Qt::AlignRight);
        table->setItem(counter, column, item);
        counter++;
    }
}

void Dialog::BrowseDate(vector<time_t>* list, map<time_t, string>* pairs,
                        int& counter, QTableWidget *table, int column)
{
    // Fill up a column by user class
    TableWidgetDateItem* item = 0;
    for (auto it = list->begin(); it != list->end(); ++it){
        map<time_t, string>::iterator pair_str = pairs->find(*it);
        QString text = QString::fromStdString(pair_str->second);

        item = new TableWidgetDateItem;
        // set time value for sorting items
        item->setTime(*it);
        item->setText(text);
        item->setFlags(item->flags() & ~Qt::ItemIsEditable);
        item->setTextAlignment(Qt::AlignRight);
        table->setItem(counter, column, item);
        counter++;
    }
}

void Dialog::BrowseNames(vector<string> *list, int &counter, QTableWidget *table, ICatalogView::Entity type)
{
    QTableWidgetItem* item = 0;
    for (auto it = list->begin(); it != list->end(); ++it){
        QString text = QString::fromStdString(*it);
        item = new QTableWidgetItem(text);
        if (type == Entity::FILE){
            item->setIcon(QPixmap(":/icons/file.png"));
        } else {
            item->setIcon(QPixmap(":/icons/folder.png"));
        }
        item->setFlags(item->flags() & ~Qt::ItemIsEditable);
        table->setItem(counter, 0, item);
        counter++;
    }
}

void Dialog::BrowseHomeNames(vector<string> *list, int &counter, ICatalogView::Entity type)
{
    // For each catalog
    // when "~" has clicked
    QTableWidgetItem* left = 0;
    QTableWidgetItem* right = 0;
    for (auto it = list->begin(); it != list->end(); ++it){
        QString text = QString::fromStdString(*it);
        left = new QTableWidgetItem(text);
        right = new QTableWidgetItem(text);
        left->setFlags(left->flags() & ~Qt::ItemIsEditable);
        right->setFlags(right->flags() & ~Qt::ItemIsEditable);
        if (type == Entity::FILE){
            left->setIcon(QPixmap(":/icons/file.png"));
            right->setIcon(QPixmap(":/icons/file.png"));
        } else {
            left->setIcon(QPixmap(":/icons/folder.png"));
            right->setIcon(QPixmap(":/icons/folder.png"));
        }
        table_l->setItem(counter, 0, left);
        table_r->setItem(counter, 0, right);
        counter++;
    }
}

void Dialog::BrowseHome(vector<string> *list, int &counter, int column)
{
    // For each catalog
    // when "~" has clicked
    QTableWidgetItem* left = 0;
    QTableWidgetItem* right = 0;
    for (auto it = list->begin(); it != list->end(); ++it){
        QString text = QString::fromStdString(*it);
        left = new QTableWidgetItem(text);
        right = new QTableWidgetItem(text);
        left->setFlags(left->flags() & ~Qt::ItemIsEditable);
        right->setFlags(right->flags() & ~Qt::ItemIsEditable);
        left->setTextAlignment(Qt::AlignRight);
        right->setTextAlignment(Qt::AlignRight);

        table_l->setItem(counter, column, left);
        table_r->setItem(counter, column, right);
        counter++;
    }
}

void Dialog::BrowseSizeHome(vector<long long> *list, map<long long, string>* pairs, int &counter, int column)
{
    // For each catalog
    // when "~" has clicked
    TableWidgetSizeItem* left = 0;
    TableWidgetSizeItem* right = 0;
    for (auto it = list->begin(); it != list->end(); ++it){
        map<long long, string>::iterator pair_str = pairs->find(*it);
        QString text = QString::fromStdString(pair_str->second);
        left = new TableWidgetSizeItem;
        right = new TableWidgetSizeItem;

        left->setText(text);
        right->setText(text);
        left->setSize(*it);
        right->setSize(*it);
        left->setFlags(left->flags() & ~Qt::ItemIsEditable);
        right->setFlags(right->flags() & ~Qt::ItemIsEditable);
        left->setTextAlignment(Qt::AlignRight);
        right->setTextAlignment(Qt::AlignRight);

        table_l->setItem(counter, column, left);
        table_r->setItem(counter, column, right);
        counter++;
    }
}

void Dialog::BrowseDateHome(vector<time_t> *list, map<time_t, string> *pairs, int &counter, int column)
{
    // For each catalog
    // when "~" has clicked
    TableWidgetDateItem* left = 0;
    TableWidgetDateItem* right = 0;
    for (auto it = list->begin(); it != list->end(); ++it){
        map<time_t, string>::iterator pair_str = pairs->find(*it);
        QString text = QString::fromStdString(pair_str->second);
        left = new TableWidgetDateItem;
        right = new TableWidgetDateItem;

        left->setText(text);
        right->setText(text);
        left->setTime(*it);
        right->setTime(*it);
        left->setFlags(left->flags() & ~Qt::ItemIsEditable);
        right->setFlags(right->flags() & ~Qt::ItemIsEditable);
        left->setTextAlignment(Qt::AlignRight);
        right->setTextAlignment(Qt::AlignRight);

        table_l->setItem(counter, column, left);
        table_r->setItem(counter, column, right);
        counter++;
    }
}


void Dialog::Form(int files, int dirs)
{
    // Get count of elements to browse
    int rows = files + dirs;
    // If cell on one of catalogs has clicked
    if (qobject_cast<QTableWidget*>(sender())){
        QTableWidget* table = qobject_cast<QTableWidget*>(sender());
        table->setRowCount(rows);
        // Disable numeration of rows
        table->verticalHeader()->setVisible(false);
    } else {
        table_l->setRowCount(rows);
        table_r->setRowCount(rows);
        table_l->verticalHeader()->setVisible(false);
        table_r->verticalHeader()->setVisible(false);
        table_l->horizontalHeader()->resizeSection(0, 150);
        table_r->horizontalHeader()->resizeSection(0, 150);
        table_l->horizontalHeader()->resizeSection(1, 80);
        table_r->horizontalHeader()->resizeSection(1, 80);
        table_l->horizontalHeader()->resizeSection(3, 150);
        table_r->horizontalHeader()->resizeSection(3, 150);
    }
}

const char *Dialog::GetPath() const
{
    // Taking a path of a table which need it
    if (QObject::sender() == table_l){
        return path_left_table.c_str();
    } else {
        return path_right_table.c_str();
    }
}

void Dialog::Reset()
{
    // Redrawing a table/tables to avoid an overlay
    if (qobject_cast<QTableWidget*>(sender())){
        QTableWidget* table = qobject_cast<QTableWidget*>(sender());
        table->setRowCount(0);
    } else {
        table_l->setRowCount(0);
        table_r->setRowCount(0);
    }
}

void Dialog::SetPath(const char *new_path)
{
    if (QObject::sender() == table_l || qobject_cast<QDialog*>(sender())){
        path_left_table = const_cast<char*>(new_path);
    } else if (QObject::sender() == table_r || qobject_cast<QDialog*>(sender())){
        path_right_table = const_cast<char*>(new_path);
    } else {
        path_left_table = const_cast<char*>(new_path);
        path_right_table = const_cast<char*>(new_path);
    }
}

void Dialog::SetNameList(vector<std::string> *names_f, vector<std::string> *names_d)
{
    // Firstly draw directories names
    // then draw files names
    int counter = 0;
    if (qobject_cast<QTableWidget*>(sender())){
            QTableWidget* table = qobject_cast<QTableWidget*>(sender());
            if (names_d->size() > 0){
                BrowseNames(names_d,counter,table,Entity::DIR);
            }
            if (names_f->size() > 0){
                BrowseNames(names_f,counter,table,Entity::FILE);
            }
    } else {
        if (names_d->size() > 0){
            BrowseHomeNames(names_d,counter,Entity::DIR);
        }
        if (names_f->size() > 0){
            BrowseHomeNames(names_f,counter,Entity::FILE);
        }
    }
}

void Dialog::SetExtensionList(vector<std::string> *extensions, int pos)
{
    // Firstly drawing directories extension
    // then draw files extension
    if (qobject_cast<QTableWidget*>(sender())){
        QTableWidget* table = qobject_cast<QTableWidget*>(sender());
        if (pos > 0){ // BUG
            // Extension of directories - <dir>
            vector<string> dir_ext;
            dir_ext.assign(pos, "<dir>");
            int start = 0;
            Browse(&dir_ext,start,table,1);
        }
        if (extensions->size() > 0)
            Browse(extensions, pos, table, 1);
    } else {
        if (pos > 0){
            // Extension of directories - <dir>
            vector<string> dir_ext;
            dir_ext.assign(pos, "<dir>");
            int start = 0;
            BrowseHome(&dir_ext,start,1);
        }
        if (extensions->size() > 0)
            BrowseHome(extensions,pos, 1);
    }

}

void Dialog::SetSizeList(vector<long long> *sizes, map<long long, std::string> *pairs, int pos)
{
    // Drawing only size of files
    if (sizes->size() > 0){
        if (qobject_cast<QTableWidget*>(sender())){
            QTableWidget* table = qobject_cast<QTableWidget*>(sender());
            BrowseSize(sizes,pairs,pos,table,2);
        } else {
            BrowseSizeHome(sizes,pairs,pos,2);
        }
    }
}

void Dialog::SetDateList(vector<time_t>* dates, map<time_t, string>* pairs)
{
    if (dates->size() > 0){
        int pos = 0;
        if (qobject_cast<QTableWidget*>(sender())){
            QTableWidget* table = qobject_cast<QTableWidget*>(sender());
            BrowseDate(dates,pairs,pos,table,3);
        } else {
            BrowseDateHome(dates,pairs,pos,3);
        }
    }
}

void Dialog::Order()
{
    // Getting an order and column of sort
    // to prevent a losing a current state of browsing
    if (qobject_cast<QTableWidget*>(sender())){
        QTableWidget* table = qobject_cast<QTableWidget*>(sender());
        Qt::SortOrder cur_order =
                table->horizontalHeader()->sortIndicatorOrder();
        int cur_column =
                table->horizontalHeader()->sortIndicatorSection();
        table->sortByColumn(cur_column, cur_order);

    } else {
        Qt::SortOrder cur_order =
                table_l->horizontalHeader()->sortIndicatorOrder();
        int cur_column =
                table_l->horizontalHeader()->sortIndicatorSection();
        table_l->sortByColumn(cur_column, cur_order);
        cur_order = table_r->horizontalHeader()->sortIndicatorOrder();
        cur_column = table_r->horizontalHeader()->sortIndicatorSection();
        table_r->sortByColumn(cur_column, cur_order);
    }
}
