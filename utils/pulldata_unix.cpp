#include "pulldata.h"
#include <regex>
#include <QDebug>
#include <sys/stat.h>
#include <dirent.h>

namespace pulldata {

// Functions definitions to pull data about files and directories
using std::string;
using std::regex;
using std::regex_match;

vector<string>* GetCapacity(const char* path){
    try{
        vector<string>* temp = new vector<string>;
        struct dirent *directory = new struct dirent;

        DIR* disk = opendir(path);
        if (disk == NULL){
            qDebug() << "Error! Can't open directory! Path:" << path;
            exit(EXIT_FAILURE);
        }
        // Filtering ".x.." and "." files
        regex dot_expr ("^[.]{1}");
        regex dotsymb_expr ("^[.]{1}[A-Za-z0-9_-]{1}.+$");
        while ((directory = readdir(disk))){
            if (regex_match(directory->d_name, dotsymb_expr)||
                    regex_match(directory->d_name, dot_expr)) continue;
            temp->push_back(directory->d_name); // string
        }
        closedir(disk);
        delete directory;
        return temp;
    } catch (std::bad_alloc& er){
        qDebug() << "[Constructor capacity]";
    }
}

bool IsFile(const char* path){
    struct stat buf;
    if ((stat(path, &buf) == 0) && (S_ISREG(buf.st_mode))){
        return true;
    } else {
        return false;
    }
}

bool IsDir(const char* path){
    struct stat buf;
    if ((stat(path, &buf) == 0) && (S_ISDIR(buf.st_mode))){
        return true;
    } else {
        return false;
    }
}

string AfterLastSymbol(const string& name, const char* symbol){
    // Finding a last "." symbol in word
    // store word after this symbol
    string extension = name;
    size_t pointer = name.find_last_of(symbol);
    if (pointer != string::npos)
        extension = name.substr(pointer + 1);
    return extension;
}

string ConcatPath(string path, string name)
{
    const char* slesh = "/";
    return (path + slesh + name);
}

string BeforeLastSymbol(const string& name, const char* symbol)
{
    // Finding a last "." symbol in word
    // store word before this symbol
    string parse_name = name;
    size_t pos = parse_name.find_last_of(symbol);
    if (pos != string::npos)
        parse_name = name.substr(0, pos);
    return parse_name;
}

std::string VerifyToDir(const std::string &name)
{
    // If entity has <dir> extension - separate it
    string verified = name;
    if (name.find("<dir>")!= string::npos){
        verified = BeforeLastSymbol(name, ".");
    }
    return verified;
}

std::string VerifyBackspace(std::string name)
{
    // On every backspace symbol add a '\'
    // for unix systems command line
    std::string parsed = name;
    size_t pos_first = 0;
    size_t pos_last;
    for (size_t i = 0; i < name.size(); ++i){
        pos_last = parsed.find_first_of(" ", pos_first);
        if (pos_last != std::string::npos){
            parsed.insert(pos_last, "\\");
        } else
            break;
        pos_first = pos_last+2;
    }
    return parsed;
}

long long GetSize(const char *path)
{
    struct stat buffer;
    if (stat(path, &buffer) == 0){
        if (S_ISDIR(buffer.st_mode)){
            return buffer.st_size;
        } else if (S_ISREG(buffer.st_mode)){
            return buffer.st_size;
        }
    } else {
        qDebug() << "Can't open file";
        return 0;
    }

}

int GetDirCapacity(const char* path)
{
    int count = 0;
    struct dirent* directory;
    DIR* disk = opendir(path);
    if (disk == NULL){
        qDebug() << "Error! Can't open directory! Path:" << path;
        exit(EXIT_FAILURE);
    }
    while((directory = readdir(disk))){
        ++count;
    }
    return count;

}
}
