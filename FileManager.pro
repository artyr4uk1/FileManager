#-------------------------------------------------
#
# Project created by QtCreator 2018-01-24T22:46:05
#
#-------------------------------------------------

QT       += core gui
QT       += widgets
QMAKE_CXXFLAGS += -std=c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FileManager
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

win32{
SOURCES += \
        main.cpp \
        dialog.cpp \
    catalogmodel_win.cpp

}
SOURCES += \
        main.cpp \
    catalogpresenter.cpp \
    ui/deletedialog.cpp \
    ui/dialog.cpp \
    ui/editdialog.cpp \
    ui/relocatedialog.cpp \
    unix/catalogmodel_unix.cpp \
    unix/filedirectory_unix.cpp \
    utils/convert.cpp \
    ui/tablewidgetdateitem.cpp \
    ui/tablewidgetsizeitem.cpp \
    utils/terminalcommands.cpp \
    ui/progressrelocate.cpp \
    utils/relocating.cpp \
    utils/pulldata_unix.cpp \
    utils/pulldata_win.cpp

HEADERS += \
    catalogmodel.h \
    catalogpresenter.h \
    filedirectory.h \
    ui/deletedialog.h \
    ui/dialog.h \
    ui/editdialog.h \
    ui/icatalogview.h \
    ui/relocatedialog.h \
    utils/pulldata.h \
    utils/convert.h \
    ui/tablewidgetsizeitem.h \
    ui/tablewidgetdateitem.h \
    utils/terminalcommands.h \
    ui/progressrelocate.h \
    utils/relocating.h

RESOURCES = resources/res.qrc \
