#ifndef CATALOGMODEL_H
#define CATALOGMODEL_H

#include "filedirectory.h"

#include <vector>
#include <string>
#include <map>
#include <unordered_map>
#include <sys/stat.h>

namespace catalogmodel {

// Contain a class provides lists of files and directories
using std::vector;
using std::string;
using std::map;


using filedirectory::File;
using filedirectory::Directory;
using filedirectory::DotDot;

class CatalogModel
{
private:
    int count_files;
    int count_dirs;
    bool exist_dotdot;
    string path;

    struct stat* buffer; // Unix
    File** file_;
    Directory** directory_;
    DotDot** dotdot_;

    vector<string>* names_f;
    vector<string>* names_d;
    vector<string>* extensions;
    vector<long long>* sizes;
    vector<time_t>* dates;
    vector<string>* pathes;


    vector<File*> container_files;
    vector<Directory*> container_dirs;
    vector<string>* capacity;

    // Containers to store a data
    map<long long, string>* container_sizes;
    map<time_t, string>* container_dates;

    // Set count of entities and fill containers
    void Initialize();
    void FillTypeCounts(const string &path);
    void FillDateTime(vector<time_t>* dirs, vector<time_t>* files);
    void FillSizeContainer();
    void FillDateContainer();
    void SetFileCollection(const char* path);
    void SetDirectoryCollection(const char* path);
    void SetDotDot(const char* path);

public:
    CatalogModel(const char* dir_path);
    ~CatalogModel();

    // Managing a memory of containers
    // especially during move in directories
    void AllocateContainres();
    void FreeContainers();

    void SetPath(const char* new_path);
    const char* GetPath();
    int GetFileCount();
    int GetDirCount();
    bool CheckDotDot();
    void Reset(); // Clear containers to prevent adding

    string OpenDir(const string& path, const string& name);
    string OpenDotDot(const string &path);
    const char *GetDotDotName();
    vector<string>* GetFileNames();
    vector<string>* GetDirNames();
    vector<string>* GetExtensions();
    vector<long long> *GetSizes();
    vector<time_t> *GetDates();
    map<long long, string> *getContainer_sizes() const;
    map<time_t, string> *getContainer_dates() const;
    vector<std::string> *GetDirPathes();
};

}
#endif // CATALOGMODEL_H
