#include "progressrelocate.h"
#include "utils/pulldata.h"
#include <QPushButton>
#include <QHBoxLayout>
#include <QDebug>

using std::string;


Progress::Progress(QWidget *pobj) : QWidget(pobj)
{
    prog = new QProgressBar;
    prog->setMinimumWidth(200);
    prog->setAlignment(Qt::AlignCenter);

    QHBoxLayout* layout = new QHBoxLayout;
    layout->addWidget(prog);
    setLayout(layout);
}

void Progress::SetMaxValue(const int &value)
{
    prog->setRange(0, value);
}

void Progress::IncreaseStep(const int& step)
{
    prog->setValue(step);
    if (step == prog->maximum())
        close();
}
