#ifndef PROGRESSRELOCATE_H
#define PROGRESSRELOCATE_H
#include <QWidget>
#include <QProgressBar>
#include <string>

class QProgressBar;

class Progress: public QWidget{
    Q_OBJECT
private:
    QProgressBar* prog;
signals:
    void Change(const int&);
public:
    Progress(/*const std::string &path_old, const std::string &path_new,*/ QWidget* pobj = 0);
    void SetMaxValue(const int& value);

//public slots:
    void IncreaseStep(const int &step);
};
#endif // PROGRESSRELOCATE_H
